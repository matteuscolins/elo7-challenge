package br.com.elo7.sonda.candidato.controllers.impl;

import br.com.elo7.sonda.candidato.controllers.ProbeController;
import br.com.elo7.sonda.candidato.models.Probe;
import br.com.elo7.sonda.candidato.services.PlanetService;
import br.com.elo7.sonda.candidato.services.ProbeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

@RestController
public class ProbeControllerImpl implements ProbeController {

    @Autowired
    ProbeService probeService;

    @Autowired
    PlanetService planetService;

    Logger log = LoggerFactory.getLogger(ProbeControllerImpl.class);

    @Override
    public ResponseEntity<Probe> create(Probe probe) {
        var probeCreated = probeService.save(probe);
        if(Objects.nonNull(probeCreated)){
            log.info("Probe created in database. Probe name = {}, x = {}, y = {}", probe.getName(), probe.getX(), probe.getY());
            return ResponseEntity.ok(probeCreated);
        }
        log.error("Error creating probe in database. Probe name = {0}, x = {1}, y = {2}");
        return ResponseEntity.badRequest().build();
    }

    @Override
    public ResponseEntity<List<Probe>> findAll() {
        var probes = probeService.findAll();
        if(probes.isEmpty()){
            log.error("Probe list is empty");
            return ResponseEntity.notFound().build();
        }
        log.info("Probe list contains {} records", probes.size());
        return ResponseEntity.ok(probes);
    }

    @Override
    public ResponseEntity<Probe> findById(Integer id) {
        return probeService.findById(id).map(probe -> {
            log.info("Data found from probe with ID = {}", id);
            return ResponseEntity.ok(probe);
        }).orElseGet(() -> {
            log.error("Probe not found for Id = {}", id);
            return ResponseEntity.notFound().build();
        });
    }

    @Override
    public ResponseEntity<Probe> landOnThePlanet(Integer idProbe, Integer idPlanet) {
        var planet = planetService.findById(idPlanet);
        var probe = probeService.findById(idProbe);
        if(planet.isPresent() && probe.isPresent()){
            var probeLanded = probeService.land(probe.get(), planet.get());
            log.info("Probe {} landed on the planet {}", probe.get().getName(), planet.get().getName());
            return ResponseEntity.ok(probeLanded);
        }
        log.error("Not found datas for planet ID = {} and/or not found data for probe ID = {}", idPlanet, idProbe);
        return ResponseEntity.notFound().build();
    }

    @Override
    public ResponseEntity<Probe> move(Integer id, String command) {
        var probe = this.probeService.move(id, command);
        if(Objects.nonNull(probe)){
            return ResponseEntity.ok(probe);
        }
        return ResponseEntity.notFound().build();
    }
}
