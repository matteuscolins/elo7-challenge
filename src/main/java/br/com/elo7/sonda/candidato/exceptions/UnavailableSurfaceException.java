package br.com.elo7.sonda.candidato.exceptions;

public class UnavailableSurfaceException extends RuntimeException {
    public UnavailableSurfaceException(String message) {
        super(message);
    }
}
