package br.com.elo7.sonda.candidato.repositories;

import br.com.elo7.sonda.candidato.models.Probe;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProbeRepository extends CrudRepository<Probe, Integer> {

    List<Probe> findByNameContaining(String name);

}
