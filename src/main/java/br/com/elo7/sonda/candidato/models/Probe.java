package br.com.elo7.sonda.candidato.models;

import br.com.elo7.sonda.candidato.enums.CommandEnum;
import br.com.elo7.sonda.candidato.enums.DirectionEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "probes")
public class Probe {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@NotBlank(message = "Probe name is required")
	private String name;
	@NotBlank(message = "Probe x is required")
	@Size(min = 1)
	private int x;
	@NotBlank(message = "Probe y is required")
	@Size(min = 1)
	private int y;
	@Enumerated(EnumType.STRING)
	private DirectionEnum direction;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "planet_id")
	private Planet planet;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public DirectionEnum getDirection() {
		return direction;
	}

	@JsonIgnore
	public int getTotalArea(){
		return x * y;
	}
	public void setDirection(DirectionEnum direction) {
		this.direction = direction;
	}
	public Planet getPlanet() {
		return planet;
	}
	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public void moveForward(){
		CommandEnum.M.move(this);
	}

	public void turnLeft() {
		CommandEnum.L.move(this);
	}

	public void turnRight() {
		CommandEnum.R.move(this);
	}
}
