package br.com.elo7.sonda.candidato.repositories;

import br.com.elo7.sonda.candidato.models.Planet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlanetRepository extends CrudRepository<Planet, Integer> {
    List<Planet> findByNameContaining(String name);
}
