package br.com.elo7.sonda.candidato.helpers;

import br.com.elo7.sonda.candidato.models.Probe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ProbeHelper {

    Logger log = LoggerFactory.getLogger(ProbeHelper.class);

    public void convertCommands(Probe probe, String commands){
        for(char command : commands.toUpperCase().toCharArray()){
            switch (command){
                case 'L':
                    probe.turnLeft();
                    break;
                case 'R':
                    probe.turnRight();
                    break;
                case 'M':
                    probe.moveForward();
                    break;
                default:
                    log.error("Unidentified command. Command = {}", command);
            }
        }
    }
}
