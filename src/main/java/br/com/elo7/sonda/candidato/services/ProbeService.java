package br.com.elo7.sonda.candidato.services;

import br.com.elo7.sonda.candidato.exceptions.UnavailableSurfaceException;
import br.com.elo7.sonda.candidato.models.Planet;
import br.com.elo7.sonda.candidato.models.Probe;

import java.util.List;
import java.util.Optional;

public interface ProbeService {
    Probe save(Probe probe);
    Optional<Probe> findById(int i);
    List<Probe> findAll();
    List<Probe> findByName(String mars);
    Probe land(Probe probe, Planet planet) throws UnavailableSurfaceException;
    Probe move(Integer id, String command);
}
