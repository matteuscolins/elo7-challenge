package br.com.elo7.sonda.candidato.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Entity
@Table(name = "planets")
public class Planet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@NotBlank(message = "Planet name is required")
	private String name;
	@NotBlank(message = "Planet width is required")
	@Size(min = 1)
	private int width;
	@NotBlank(message = "Planet height is required")
	@Size(min = 1)
	private int height;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "planet", cascade = CascadeType.ALL)
	private List<Probe> probes = new ArrayList<>();
	
	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Planet) {
			return ((Planet) obj).id == this.id;
		}
		return false;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public List<Probe> getProbes() {
		return probes;
	}
	public void setProbes(List<Probe> probes) {
		this.probes = probes;
	}

	@JsonIgnore
	public int getSurfaceAvailable(){
		int totalPlanetArea = width * height;
		AtomicInteger totalProbesArea = new AtomicInteger();
		if(this.getProbes().isEmpty()){
			return totalPlanetArea;
		}
		this.probes.forEach(probe -> totalProbesArea.set(totalProbesArea.get() + (probe.getX() * probe.getY())));
		return totalPlanetArea - totalProbesArea.get();
	}
}
