package br.com.elo7.sonda.candidato.controllers.impl;

import br.com.elo7.sonda.candidato.controllers.PlanetController;
import br.com.elo7.sonda.candidato.models.Planet;
import br.com.elo7.sonda.candidato.services.PlanetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

@RestController
public class PlanetControllerImpl implements PlanetController {

    @Autowired
    PlanetService planetService;

    Logger log = LoggerFactory.getLogger(PlanetControllerImpl.class);

    @Override
    public ResponseEntity<Planet> create(Planet planet) {
        var planetCreted = planetService.save(planet);
        if(Objects.nonNull(planetCreted)){
            log.info("{} planet created in database", planetCreted.getName());
            return ResponseEntity.ok(planetCreted);
        }
        log.error("Error creating planet");
        return ResponseEntity.badRequest().build();
    }

    @Override
    public ResponseEntity<List<Planet>> findAll() {
        var planets = planetService.findAll();
        if(planets.isEmpty()){
            log.error("Planets not found");
            return ResponseEntity.notFound().build();
        }
        log.info("Found {} records of planets registers", planets.size());
        return ResponseEntity.ok(planetService.findAll());
    }

    @Override
    public ResponseEntity<Planet> findById(Integer id) {
        var planet = planetService.findById(id);
        return planet.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }
}
