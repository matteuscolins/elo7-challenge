package br.com.elo7.sonda.candidato.controllers;

import br.com.elo7.sonda.candidato.models.Planet;
import br.com.elo7.sonda.candidato.models.Probe;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@RequestMapping("/probes")
public interface ProbeController {

    @Operation(summary = "Create probe in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Probe created in database",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Probe.class)) }),
            @ApiResponse(responseCode = "400", description = "Error creating probe",
                    content = @Content) })
    @PostMapping
    ResponseEntity<Probe> create(@RequestBody Probe probe);

    @Operation(summary = "Get all probes in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found all probes",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Probe.class)) }),
            @ApiResponse(responseCode = "404", description = "Probes not found",
                    content = @Content) })
    @GetMapping
    ResponseEntity<List<Probe>> findAll();

    @Operation(summary = "Get probe by id in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found probe by id",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Probe.class)) }),
            @ApiResponse(responseCode = "404", description = "Probe not found",
                    content = @Content) })
    @GetMapping("/{id}")
    ResponseEntity<Probe> findById(@PathVariable Integer id);

    @Operation(summary = "Land probe in planet")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Landing probe in planet",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Probe.class)) }),
            @ApiResponse(responseCode = "404", description = "Probe and/or planet not found",
                    content = @Content) })
    @PostMapping("/land/{idProbe}/planet/{idPlanet}")
    ResponseEntity<Probe> landOnThePlanet(@PathVariable Integer idProbe, @PathVariable Integer idPlanet);

    @Operation(summary = "Move probe in planet")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Move probe in planet",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Probe.class)) }),
            @ApiResponse(responseCode = "404", description = "Probe and/or planet not found",
                    content = @Content) })
    @PostMapping("/{id}/move")
    ResponseEntity<Probe> move(@PathVariable Integer id, @RequestParam String command);
}
