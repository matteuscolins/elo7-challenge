package br.com.elo7.sonda.candidato.services.impl;

import br.com.elo7.sonda.candidato.models.Planet;
import br.com.elo7.sonda.candidato.repositories.PlanetRepository;
import br.com.elo7.sonda.candidato.services.PlanetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlanetServiceImpl implements PlanetService {

    @Autowired
    PlanetRepository planetRepository;

    @Override
    public Planet save(Planet planet) {
        return planetRepository.save(planet);
    }

    @Override
    public Optional<Planet> findById(int id) {
        return planetRepository.findById(id);
    }

    @Override
    public List<Planet> findAll() {
        return (List<Planet>) planetRepository.findAll();
    }

    @Override
    public List<Planet> findByName(String name) {
        return planetRepository.findByNameContaining(name);
    }

    @Override
    public Planet update(int id, Planet planet) {
        var planetUpdated = findById(id).map(p -> {
            p.setName(planet.getName());
            p.setHeight(planet.getHeight());
            p.setWidth(planet.getWidth());
            return p;
        }).orElseThrow();
        return save(planetUpdated);
    }
}
