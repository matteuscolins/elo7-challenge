package br.com.elo7.sonda.candidato.services.impl;

import br.com.elo7.sonda.candidato.exceptions.UnavailableSurfaceException;
import br.com.elo7.sonda.candidato.helpers.ProbeHelper;
import br.com.elo7.sonda.candidato.models.Planet;
import br.com.elo7.sonda.candidato.models.Probe;
import br.com.elo7.sonda.candidato.repositories.ProbeRepository;
import br.com.elo7.sonda.candidato.services.PlanetService;
import br.com.elo7.sonda.candidato.services.ProbeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;

@Service
public class ProbeServiceImpl implements ProbeService {

    @Autowired
    private ProbeRepository probeRepository;

    @Autowired
    private PlanetService planetService;

    @Autowired
    private ProbeHelper probeHelper;

    @Override
    public Probe save(Probe probe) {
        return probeRepository.save(probe);
    }

    @Override
    public Optional<Probe> findById(int id) {
        return probeRepository.findById(id);
    }

    @Override
    public List<Probe> findAll() {
        return (List<Probe>) probeRepository.findAll();
    }

    @Override
    public List<Probe> findByName(String name) {
        return probeRepository.findByNameContaining(name);
    }

    @Override
    public Probe land(Probe probe, Planet planet) throws UnavailableSurfaceException {
        if(probe.getTotalArea() > planet.getSurfaceAvailable()){
            throw new UnavailableSurfaceException(MessageFormat.format("Unavailable surface on {0} for {1} landing", planet.getName(), probe.getName()));
        }
        probe.setPlanet(planet);
        save(probe);
        return save(probe);
    }

    @Override
    public Probe move(Integer id, String command) {
        var probe = findById(id);
        if(probe.isPresent()){
            probeHelper.convertCommands(probe.get(), command);
            return save(probe.get());
        }
        return null;
    }
}
