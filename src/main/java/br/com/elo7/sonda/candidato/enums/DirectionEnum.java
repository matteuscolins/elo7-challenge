package br.com.elo7.sonda.candidato.enums;

import br.com.elo7.sonda.candidato.models.Probe;

public enum DirectionEnum {

    N{
        @Override
        public void moveToLeft(Probe probe) {
            probe.setDirection(DirectionEnum.W);
        }

        @Override
        public void moveToRight(Probe probe) {
            probe.setDirection(DirectionEnum.E);
        }

        @Override
        public void moveForward(Probe probe) {
            var newY = probe.getY();
            newY++;
            probe.setY(newY);
        }
    },
    W{
        @Override
        public void moveToLeft(Probe probe) {
            probe.setDirection(DirectionEnum.S);
        }

        @Override
        public void moveToRight(Probe probe) {
            probe.setDirection(DirectionEnum.N);
        }

        @Override
        public void moveForward(Probe probe) {
            var newX = probe.getX();
            newX--;
            probe.setX(newX);
        }
    },
    E{
        @Override
        public void moveToLeft(Probe probe) {
            probe.setDirection(DirectionEnum.N);
        }

        @Override
        public void moveToRight(Probe probe) {
            probe.setDirection(DirectionEnum.S);
        }

        @Override
        public void moveForward(Probe probe) {
            var newX = probe.getX();
            newX++;
            probe.setX(newX);
        }
    },
    S{
        @Override
        public void moveToLeft(Probe probe) {
            probe.setDirection(DirectionEnum.E);
        }

        @Override
        public void moveToRight(Probe probe) {
            probe.setDirection(DirectionEnum.W);
        }

        @Override
        public void moveForward(Probe probe) {
            var newY = probe.getY();
            newY--;
            probe.setY(newY);
        }
    };

    public abstract void moveToLeft(Probe probe);
    public abstract void moveToRight(Probe probe);
    public abstract void moveForward(Probe probe);
}
