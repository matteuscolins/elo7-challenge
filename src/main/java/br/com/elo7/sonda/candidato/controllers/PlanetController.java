package br.com.elo7.sonda.candidato.controllers;

import br.com.elo7.sonda.candidato.models.Planet;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping("/planets")
public interface PlanetController {

    @Operation(summary = "Create planet in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Planet created",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Planet.class)) }),
            @ApiResponse(responseCode = "404", description = "Planets not found",
                    content = @Content) })
    @PostMapping
    ResponseEntity<Planet> create(@RequestBody Planet planet);

    @Operation(summary = "Get all planets in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found all planets",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Planet.class)) }),
            @ApiResponse(responseCode = "404", description = "Planets not found",
                    content = @Content) })
    @GetMapping
    ResponseEntity<List<Planet>> findAll();

    @Operation(summary = "Get planet by id in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the planet",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Planet.class)) }),
            @ApiResponse(responseCode = "404", description = "Planet not found",
                    content = @Content) })
    @GetMapping("/{id}")
    ResponseEntity<Planet> findById(@Parameter(description = "Id of planet to be searched") @PathVariable Integer id);
}
