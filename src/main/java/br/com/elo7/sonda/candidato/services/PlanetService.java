package br.com.elo7.sonda.candidato.services;

import br.com.elo7.sonda.candidato.models.Planet;

import java.util.List;
import java.util.Optional;

public interface PlanetService {
    Planet save(Planet planet);

    Optional<Planet> findById(int id);

    List<Planet> findAll();

    List<Planet> findByName(String name);

    Planet update(int id, Planet planetUpdated);
}
