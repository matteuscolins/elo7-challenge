package br.com.elo7.sonda.candidato.enums;

import br.com.elo7.sonda.candidato.models.Probe;

public enum CommandEnum {

    L{
        @Override
        public void move(Probe probe) {
            probe.getDirection().moveToLeft(probe);
        }
    },
    R{
        @Override
        public void move(Probe probe) {
            probe.getDirection().moveToRight(probe);
        }
    },
    M{
        @Override
        public void move(Probe probe) {
            probe.getDirection().moveForward(probe);
        }
    };

    public abstract void move(Probe probe);
}
