package br.com.elo7.sonda.candidato.services;

import br.com.elo7.sonda.candidato.models.Planet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class PlanetServiceTest {

    @Autowired
    PlanetService planetService;

    Planet marsPlanet;

    @BeforeEach
    void init(){
        marsPlanet = new Planet();
        marsPlanet.setId(1);
        marsPlanet.setName("Mars");
        marsPlanet.setWidth(5);
        marsPlanet.setHeight(5);

        var neptunePlanet = new Planet();
        neptunePlanet.setId(3);
        neptunePlanet.setName("Neptune");
        neptunePlanet.setWidth(2);
        neptunePlanet.setHeight(2);

        planetService.save(marsPlanet);
        planetService.save(neptunePlanet);
    }

    @Test
    void should_save_planet_in_database(){
        var planet = new Planet();
        planet.setId(2);
        planet.setName("Venus");
        planet.setWidth(10);
        planet.setHeight(10);
        Assertions.assertNotNull(planetService.save(planet));
    }

    @Test
    void should_return_planet_when_call_find_by_id(){
        var planet = planetService.findById(1);
        Assertions.assertTrue(planet.isPresent());
    }

    @Test
    void should_return_empty_when_call_find_by_id(){
        var planet = planetService.findById(1000);
        Assertions.assertTrue(planet.isEmpty());
    }

    @Test
    void should_return_all_planets_when_call_find_all(){
        List<Planet> planets = planetService.findAll();
        Assertions.assertNotNull(planets);
    }

    @Test
    void should_return_planet_list_filter_by_name_when_call_find_by_name(){
        List<Planet> planets = planetService.findByName("Neptune");
        Assertions.assertNotNull(planets);
    }

    @Test
    void should_return_planet_list_empty_filter_by_name_when_call_find_by_name(){
        List<Planet> planets = planetService.findByName("Jupiter");
        Assertions.assertTrue(planets.isEmpty());
    }

    @Test
    void should_return_planet_updated_when_call_update(){
        Planet planet = new Planet();
        planet.setHeight(45);
        planet.setWidth(45);
        var planetUpdated = planetService.update(1, planet);
        Assertions.assertEquals(45, planetUpdated.getHeight());
        Assertions.assertEquals(45, planetUpdated.getWidth());
    }

}