package br.com.elo7.sonda.candidato.controllers;

import br.com.elo7.sonda.candidato.enums.DirectionEnum;
import br.com.elo7.sonda.candidato.models.Planet;
import br.com.elo7.sonda.candidato.models.Probe;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

@SpringBootTest
class ProbeControllerTest {

    @Autowired
    private ProbeController probeController;

    @Autowired
    private PlanetController planetController;

    private Planet marsPlanet;
    private Probe zond1;

    @BeforeEach
    void init(){
        zond1 = new Probe();
        zond1.setName("Zond 1");
        zond1.setDirection(DirectionEnum.N);
        zond1.setY(3);
        zond1.setX(3);
        this.probeController.create(zond1);

        marsPlanet = new Planet();
        marsPlanet.setName("Mars");
        marsPlanet.setWidth(5);
        marsPlanet.setHeight(5);
        this.planetController.create(marsPlanet);
    }

    @Test
    void should_return_status_ok_when_call_create(){
        Probe venera6 = new Probe();
        venera6.setId(1);
        venera6.setName("Venera 6");

        venera6.setY(3);
        venera6.setX(3);
        var response = this.probeController.create(venera6);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void should_return_status_ok_call_find_all(){
        var response = this.probeController.findAll();
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void should_return_body_not_empty_when_call_find_all(){
        var response = probeController.findAll();
        Assertions.assertFalse(response.getBody().isEmpty());
    }

    @Test
    void should_return_status_ok_when_call_find_by_id(){
        var response = probeController.findById(1);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void should_return_status_not_found_when_call_find_by_id(){
        var response = probeController.findById(1000);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void shoud_return_status_code_ok_when_call_land_on_the_planet() {
        var response = probeController.landOnThePlanet(1, 1);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void shoud_return_status_code_not_found_when_call_land_on_the_planet() {
        var response = probeController.landOnThePlanet(1000, 1000);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void should_retun_status_code_ok_when_call_move(){
        var response = probeController.move(1, "LMLMLMLMM");
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}