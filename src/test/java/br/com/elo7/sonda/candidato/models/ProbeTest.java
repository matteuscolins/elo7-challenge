package br.com.elo7.sonda.candidato.models;

import br.com.elo7.sonda.candidato.enums.DirectionEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ProbeTest {

    private Probe probe;

    @BeforeEach
    void init(){
        probe = new Probe();
        probe.setId(1);
        probe.setName("Venera 1965A");
        probe.setY(1);
        probe.setX(1);
    }

    @Test
    void should_return_venera_1965a_when_call_get_name(){
        Assertions.assertEquals("Venera 1965A", probe.getName());
    }

    @Test
    void should_return_1_when_call_get_y(){
        Assertions.assertEquals(1, probe.getY());
    }

    @Test
    void should_return_1_when_call_get_x(){
        Assertions.assertEquals(1, probe.getX());
    }

    @Test
    void should_return_y_equals_2_when_call_move_forward_and_direction_equals_n() {
        probe.setDirection(DirectionEnum.N);
        probe.moveForward();
        Assertions.assertEquals(2, probe.getY());
    }

    @Test
    void should_return_x_equals_0_when_call_move_forward_and_direction_equals_w() {
        probe.setDirection(DirectionEnum.W);
        probe.moveForward();
        Assertions.assertEquals(0, probe.getX());
    }

    @Test
    void should_return_y_equals_0_when_call_move_forward_and_direction_equals_s() {
        probe.setDirection(DirectionEnum.S);
        probe.moveForward();
        Assertions.assertEquals(0, probe.getY());
    }

    @Test
    void should_return_x_equals_2_when_call_move_forward_and_direction_equals_e() {
        probe.setDirection(DirectionEnum.E);
        probe.moveForward();
        Assertions.assertEquals(2, probe.getX());
    }

    @Test
    void should_return_new_direction_equals_w_when_call_turn_left_and_direction_equals_n(){
        probe.setDirection(DirectionEnum.N);
        probe.turnLeft();
        Assertions.assertEquals(DirectionEnum.W, probe.getDirection());
    }

    @Test
    void should_return_new_direction_equals_s_when_call_turn_left_and_direction_equals_w(){
        probe.setDirection(DirectionEnum.W);
        probe.turnLeft();
        Assertions.assertEquals(DirectionEnum.S, probe.getDirection());
    }

    @Test
    void should_return_new_direction_equals_e_when_call_turn_left_and_direction_equals_s(){
        probe.setDirection(DirectionEnum.S);
        probe.turnLeft();
        Assertions.assertEquals(DirectionEnum.E, probe.getDirection());
    }

    @Test
    void should_return_new_direction_equals_n_when_call_turn_left_and_direction_equals_e(){
        probe.setDirection(DirectionEnum.E);
        probe.turnLeft();
        Assertions.assertEquals(DirectionEnum.N, probe.getDirection());
    }

    @Test
    void should_return_new_direction_equals_e_when_call_turn_right_and_direction_equals_n(){
        probe.setDirection(DirectionEnum.N);
        probe.turnRight();
        Assertions.assertEquals(DirectionEnum.E, probe.getDirection());
    }

    @Test
    void should_return_new_direction_equals_s_when_call_turn_right_and_direction_equals_e(){
        probe.setDirection(DirectionEnum.E);
        probe.turnRight();
        Assertions.assertEquals(DirectionEnum.S, probe.getDirection());
    }

    @Test
    void should_return_new_direction_equals_w_when_call_turn_right_and_direction_equals_s(){
        probe.setDirection(DirectionEnum.S);
        probe.turnRight();
        Assertions.assertEquals(DirectionEnum.W, probe.getDirection());
    }

    @Test
    void should_return_new_direction_equals_n_when_call_turn_right_and_direction_equals_w(){
        probe.setDirection(DirectionEnum.W);
        probe.turnRight();
        Assertions.assertEquals(DirectionEnum.N, probe.getDirection());
    }
}