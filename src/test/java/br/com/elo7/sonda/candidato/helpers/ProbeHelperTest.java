package br.com.elo7.sonda.candidato.helpers;

import br.com.elo7.sonda.candidato.enums.DirectionEnum;
import br.com.elo7.sonda.candidato.models.Probe;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ProbeHelperTest {

    @Autowired
    ProbeHelper probeHelper;

    Probe probe;

    @BeforeEach
    void init(){
        probe = new Probe();
        probe.setId(1);
        probe.setName("Zond 1");
        probe.setDirection(DirectionEnum.N);
        probe.setY(2);
        probe.setX(1);
    }

    @Test
    void should_return_probe_with_direction_north_after_convert_commands_LMLMLMLMM() {
        probeHelper.convertCommands(probe, "LMLMLMLMM");
        Assertions.assertEquals(DirectionEnum.N, probe.getDirection());
    }

    @Test
    void should_return_probe_with_x_equals_1_after_convert_commands_LMLMLMLMM() {
        probeHelper.convertCommands(probe, "LMLMLMLMM");
        Assertions.assertEquals(1, probe.getX());
    }

    @Test
    void should_return_probe_with_y_equals_3_after_convert_commands_LMLMLMLMM() {
        probeHelper.convertCommands(probe, "LMLMLMLMM");
        Assertions.assertEquals(3, probe.getY());
    }

}