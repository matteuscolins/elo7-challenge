package br.com.elo7.sonda.candidato.services;

import br.com.elo7.sonda.candidato.enums.DirectionEnum;
import br.com.elo7.sonda.candidato.exceptions.UnavailableSurfaceException;
import br.com.elo7.sonda.candidato.models.Planet;
import br.com.elo7.sonda.candidato.models.Probe;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ProbeServiceTest {

    @Autowired
    private ProbeService probeService;

    @Autowired
    private PlanetService planetService;

    private Probe probe;
    private Planet marsPlanet;

    @BeforeEach
    void init(){
        marsPlanet = new Planet();
        marsPlanet.setId(1);
        marsPlanet.setName("Mars");
        marsPlanet.setWidth(5);
        marsPlanet.setHeight(5);

        planetService.save(marsPlanet);

        probe = new Probe();
        probe.setId(1);
        probe.setName("Zond 1");
        probe.setDirection(DirectionEnum.N);
        probe.setY(3);
        probe.setX(3);

        var venera6 = new Probe();
        venera6.setId(6);
        venera6.setName("Venera 6");
        venera6.setDirection(DirectionEnum.N);
        venera6.setY(3);
        venera6.setX(3);

        var venera7 = new Probe();
        venera7.setId(7);
        venera7.setName("Venera 7");
        venera7.setDirection(DirectionEnum.N);
        venera7.setY(2);
        venera7.setX(2);

        probeService.save(probe);
        probeService.save(venera6);
        probeService.save(venera7);
    }

    @Test
    void should_save_probe_in_database(){
        var probe = new Probe();
        probe.setId(2);
        probe.setName("Mariner 1");
        probe.setY(1);
        probe.setX(1);
        Assertions.assertNotNull(probeService.save(probe));
    }

    @Test
    void shoudl_return_all_probes_call_find_all(){
        List<Probe> probes = probeService.findAll();
        Assertions.assertNotNull(probes);
    }

    @Test
    void should_return_probe_when_call_find_by_id(){
        var probe = probeService.findById(1);
        Assertions.assertTrue(probe.isPresent());
    }

    @Test
    void should_return_empty_when_call_find_by_id(){
        var probe = probeService.findById(100);
        Assertions.assertTrue(probe.isEmpty());
    }

    @Test
    void should_return_list_probes_by_name_when_call_find_by_name(){
        List<Probe> probes = probeService.findByName("Venera");
        Assertions.assertNotNull(probes);
    }

    @Test
    void should_return_empty_list_by_name_when_call_find_by_name(){
        List<Probe> probes = probeService.findByName("Explorer");
        Assertions.assertTrue(probes.isEmpty());
        Assertions.assertEquals(0, probes.size());
    }

    @Test
    void should_land_probe_in_planet() throws UnavailableSurfaceException {
        var venera6 = new Probe();
        venera6.setName("Venera 6");
        venera6.setY(3);
        venera6.setX(3);

        marsPlanet.setProbes(List.of(venera6));

        var probeLanded = probeService.land(venera6, marsPlanet);

        Assertions.assertNotNull(probeLanded.getPlanet());
    }

    @Test
    void should_throw_unavailable_surface_exception_when_call_land(){
        probe.setX(10);
        probe.setY(10);
        Assertions.assertThrows(UnavailableSurfaceException.class, () -> probeService.land(probe, marsPlanet));
    }

    @Test
    void should_change_position_probe_when_call_move(){
        var probe = probeService.move(1, "LMLMLMLMM");
        Assertions.assertEquals(3, probe.getX());
        Assertions.assertEquals(4, probe.getY());
        Assertions.assertEquals(DirectionEnum.N, probe.getDirection());
    }
}