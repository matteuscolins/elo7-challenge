package br.com.elo7.sonda.candidato.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class PlanetTest {

    private Planet planet;
    private Probe probe1;
    private Probe probe2;

    @BeforeEach
    void init(){
        planet = new Planet();
        planet.setId(1);
        planet.setName("Mars");
        planet.setHeight(5);
        planet.setWidth(5);

        probe1 = new Probe();
        probe1.setId(1);
        probe1.setName("Genesis");
        probe1.setY(2);
        probe1.setX(2);

        probe2 = new Probe();
        probe2.setName("Cosmos 27");
        probe2.setId(2);
        probe2.setY(3);
        probe2.setX(3);
    }

    @Test
    void should_return_1_when_call_get_id() {
        Assertions.assertEquals(1, planet.getId());
    }

    @Test
    void should_return_mars_when_call_get_name(){
        Assertions.assertEquals("Mars", planet.getName());
    }

    @Test
    void should_return_5_when_call_get_width() {
        Assertions.assertEquals(5, planet.getWidth());
    }

    @Test
    void should_return_5_when_call_get_height() {
        Assertions.assertEquals(5, planet.getHeight());
    }

    @Test
    void should_return_2_probes_call_get_probes() {
        planet.setProbes(List.of(probe1, probe2));
        Assertions.assertEquals(2, planet.getProbes().size());
    }

    @Test
    void should_return_probes_call_get_probes(){
        planet.setProbes(List.of(probe1, probe2));
        Assertions.assertNotNull(planet.getProbes());
    }

    @Test
    void should_return_total_surface_available_when_call_get_surface_available(){
        planet.setProbes(List.of(probe1, probe2));
        Assertions.assertEquals(12, planet.getSurfaceAvailable());
    }

}