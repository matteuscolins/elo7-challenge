package br.com.elo7.sonda.candidato.controllers;

import br.com.elo7.sonda.candidato.models.Planet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

@SpringBootTest
class PlanetControllerTest {

    @Autowired
    PlanetController planetController;

    Planet mercuryPlanet;

    @BeforeEach
    void init(){
        mercuryPlanet = new Planet();
        mercuryPlanet.setName("Mercury");
        mercuryPlanet.setWidth(20);
        mercuryPlanet.setHeight(20);
        planetController.create(mercuryPlanet);
    }

    @Test
    void should_return_request_with_status_ok_when_call_create(){
        Planet uranusPlanet = new Planet();
        uranusPlanet.setName("Uranus");
        uranusPlanet.setWidth(20);
        uranusPlanet.setHeight(20);
        var response = planetController.create(uranusPlanet);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void should_return_request_with_status_ok_when_call_find_all(){
        var response = planetController.findAll();
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void should_return_request_with_status_ok_when_call_find_by_id(){
        var response = planetController.findById(2);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void should_return_request_with_status_not_found_when_call_find_by_id(){
        var response = planetController.findById(100);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}